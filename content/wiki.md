---
title: "Wiki"
description: "User Group Wiki with helpful tips and tricks for reference during and after meetings."
ShowReadingTime: false

# 'Under Construction' cover image
cover:
  image: "/images/nix-under-construction.svg"
  caption: "This page is still under construction."
  alt: "Nix snowflake logo with a stick figure construction worker shovelling, placed inthe middle of the snowflake"
  hidden: false
---
