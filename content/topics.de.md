---
title: "Themen"
description: "Liste der Themen, die bei den Treffen der User Group besprochen wurden, mit Links zu den entsprechenden Projektseiten, Repositories oder relevanten Lernmaterialien."
ShowReadingTime: false

# 'Under Construction' cover image
cover:
  image: "/images/nix-under-construction.svg"
  caption: "Diese Seite befindet sich noch in Bearbeitung."
  alt: "Nix-Schneeflocken-Logo mit einem Strichmännchen als schaufelnder Bauarbeiter in der Mitte der Schneeflocke"
  hidden: false
---


- [NixOS](https://nixos.org/)
- [Nix Documentation](https://nix.dev/)
- [Home Manager](https://nix-community.github.io/home-manager/index.html)
